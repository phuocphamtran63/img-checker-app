import type { Config } from 'tailwindcss'

export default <Partial<Config>>{
  darkMode: 'class',
  theme: {
    screens: {},
    extend: {},
  },
  variants: {},
  plugins: [],
  purge: {},
}
