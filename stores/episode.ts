import { defineStore } from 'pinia'
import { RESULT_SCREEN_MODES } from '~/constants'
// Interface
interface episodeState {
  currentResultMode: number
}
// Option Stores
export const useEpisode = defineStore('episode', {
  state: (): episodeState => ({
    currentResultMode: RESULT_SCREEN_MODES.CHECK_DIFF,
  }),
  actions: {},
})
