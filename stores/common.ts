import { defineStore } from 'pinia'
// Interface
interface commonState {
  isHeaderInside: boolean
}
// Option Stores
export const useCommon = defineStore('common', {
  state: (): commonState => ({
    isHeaderInside: false,
  }),
  actions: {},
})
