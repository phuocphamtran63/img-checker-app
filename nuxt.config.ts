// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  devtools: { enabled: true },
  modules: [
    '@nuxt/ui',
    'nuxt-local-iconify',
  ],
  build: {
    transpile: ['vuetify', '@vueuse/nuxt'],
  },
  localIconify: {
    iconPath: './assets/icons',
  },
  ssr: false,
})
